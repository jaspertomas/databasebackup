/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import models.Settings;

/**
 *
 * @author jaspertomas
 */
public class DatabaseBackupTool {
    public static boolean backupDB(String dbName, String dbUserName, String dbPassword, String path) {
        ArrayList<String> executeCmd=new ArrayList<String>();
        executeCmd.add(Settings.getByName("mysqldumppath").value+"/mysqldump");
        executeCmd.add("-u");
        executeCmd.add(dbUserName);
        if(dbPassword.length()!=0)
            executeCmd.add("-p"+dbPassword);
        executeCmd.add("--add-drop-database");
        executeCmd.add("-B");
        executeCmd.add(dbName);
        executeCmd.add("-r");
        executeCmd.add(path);
        //this is just to help convert executeCmd into a String[]
        String[] sampleStringArray={};
        
        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(executeCmd.toArray(sampleStringArray));
            int processComplete = runtimeProcess.waitFor();
 
            if (processComplete == 0) {
                System.out.println("Backup created successfully");
                return true;
            } else {
                System.out.println("Could not create the backup");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
 
        return false;
    }
    
}
