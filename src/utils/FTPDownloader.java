/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author jaspertomas
 */
public class FTPDownloader {
    
    FTPClient ftp = null;

    public FTPDownloader(String host, String user, String pwd) throws Exception{
            ftp = new FTPClient();
            ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
            int reply;
            ftp.connect(host);
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                    ftp.disconnect();
                    throw new Exception("Exception in connecting to FTP Server");
            }
            ftp.login(user, pwd);
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.enterLocalPassiveMode();
    }
    public void downloadFile(String localFileFullName, String fileName, String hostDir) throws Exception 
    {
        try(OutputStream output = new FileOutputStream(localFileFullName))
        {
            Long size=getFileSize(ftp, hostDir + fileName);

            System.out.println("File Size: "+size);
            ProgressTracker.setSizeAndReset(size);

            // do some work to initialize size
            CountingOutputStream cos = new CountingOutputStream(output){
                @Override
                protected void beforeWrite(int n){
                    super.beforeWrite(n);
                    ProgressTracker.reportProgress(getCount());
                }
            };
            
            this.ftp.retrieveFile(hostDir + fileName, cos);
        }
    }

    public void disconnect(){
            if (this.ftp.isConnected()) {
                    try {
                            this.ftp.logout();
                            this.ftp.disconnect();
                    } catch (IOException f) {
                            // do nothing as file is already saved to server
                    }
            }
    }    
    
    private long getFileSize(FTPClient ftp, String filePath) throws Exception {
        long fileSize = 0;
        FTPFile[] files = ftp.listFiles(filePath);
        if (files.length == 1 && files[0].isFile()) {
            fileSize = files[0].getSize();
        }
        return fileSize;
    }
}
