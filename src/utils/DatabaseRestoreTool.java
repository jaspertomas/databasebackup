/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.IOException;
import java.util.ArrayList;
import models.Settings;

/**
 *
 * @author jaspertomas
 */
public class DatabaseRestoreTool {
   public static boolean restoreDB(String dbName, String dbUserName, String dbPassword, String path) {
        ArrayList<String> executeCmd=new ArrayList<String>();
        executeCmd.add(Settings.getByName("mysqldumppath").value+"/mysql");
        executeCmd.add("-u");
        executeCmd.add(dbUserName);
        if(dbPassword.length()!=0)
            executeCmd.add("-p" + dbPassword);
        executeCmd.add("-e");
        executeCmd.add("source restore.sql");
        //this is just to help convert executeCmd into a String[]
        String[] sampleStringArray={};

        Process runtimeProcess;
        try {
 
            runtimeProcess = Runtime.getRuntime().exec(executeCmd.toArray(sampleStringArray));
            int processComplete = runtimeProcess.waitFor();
 
            if (processComplete == 0) {
                System.out.println("Database restored successfully");
                return true;
            } else {
                System.out.println("Database could not be restored");
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Database could not be restored. "+ex.getMessage());
            //ex.printStackTrace();
        }

        return false;
   }
     
}
