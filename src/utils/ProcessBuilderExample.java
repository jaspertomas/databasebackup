/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * from https://alvinalexander.com/java/java-exec-processbuilder-process-2
 */
import java.io.IOException;
import java.util.*;

public class ProcessBuilderExample
{
  
  public static void main(String[] args) throws Exception
  {
    new ProcessBuilderExample();
  }

  // can run basic ls or ps commands
  // can run command pipelines
  // can run sudo command if you know the password is correct
  public ProcessBuilderExample() 
  throws IOException, InterruptedException
  {
    // determine the number of processes running on the current
    // linux, unix, or mac os x system.
    //mysqldump -u root -h localhost -ppassword tmcprogram4 --skip-add-locks --skip-comments  > tmcprogram4.sql
    List<String> commands = new ArrayList<String>();
    commands.add("/usr/local/mysql-5.5.27-osx10.6-x86_64/bin/mysqldump");
    commands.add("-u root -ppassword tmcprogram4");
    
    SystemCommandExecutor commandExecutor = new SystemCommandExecutor(commands);
    int result = commandExecutor.executeCommand();

    // stdout and stderr of the command are returned as StringBuilder objects
    StringBuilder stdout = commandExecutor.getStandardOutputFromCommand();
    StringBuilder stderr = commandExecutor.getStandardErrorFromCommand();
    System.out.println("The numeric result of the command was: " + result);
    System.out.println("\nSTDOUT:");
    System.out.println(stdout);
    System.out.println("\nSTDERR:");
    System.out.println(stderr);
  }
}
