/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author jaspertomas
 */
public class ProgressTracker {
    static long size=0;
    static int progress=0;
    static int percent=0;
    
    public static void setSizeAndReset(long size)
    {
        ProgressTracker.size=size;
        progress=0;
        percent=0;
    }
    public static void reportProgress(int progress)
    {
        ProgressTracker.progress=progress;
        //System.out.println(progress);
        int newpercent=Double.valueOf(100d*progress/size).intValue();
        if(newpercent>percent)
        {
            percent=newpercent;
            System.out.println(percent+"%");
        }
    }
    
}
