/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import databasebackup.FrmMain;
import java.io.File;
import models.Settings;
import static threads.AutoBackupThread.running;
import utils.DatabaseRestoreTool;
import utils.FTPDownloader;
import utils.fileaccess.FileReader;

/**
 *
 * @author jaspertomas
 */
public class RestoreThread implements Runnable{
    String database;

    public RestoreThread(String database) {
        this.database = database;
    }
    
    @Override
    public void run() {
            //prevent two backup/restore threads from running at once
            if(running)
            {
                System.out.println("Another process is already running");
                return;
            }

            running=true;
        
            downloadAndRestore(database);
            
            //reenable buttons in form main
            FrmMain.getInstance().setButtonsEnabled(true);

            running=false;
    }
    public static void downloadAndRestore(String database)
    {
        String username=Settings.getByName("dbusername").value;
        String password=Settings.getByName("dbpassword").value;
        
        String local_filename=System.getProperty("user.dir")+"/restore.sql";
        String remote_filename="";
        String remote_dir="./";
        String index_filename=database+".txt";
        String local_index_filename=System.getProperty("user.dir")+"/"+index_filename;
        
        String ftp_password=Settings.getByName("ftppassword").value;
        String ftp_host=Settings.getByName("ftpserver").value;
        String ftp_user=Settings.getByName("ftpusername").value;

        try {
		System.out.println("Start");
		System.out.println("Downloading database backup");
                
                //delete old file if exists
                File existingfile=new File(local_filename);
                if(existingfile.exists())existingfile.delete();

                FTPDownloader ftpDownloader = new FTPDownloader(ftp_host, ftp_user, ftp_password);
		//FTP server path is relative. So if FTP account HOME directory is "/home/pankaj/public_html/" and you need to upload 
		// files to "/home/pankaj/public_html/wp-content/uploads/image2/", you should pass directory parameter as "/wp-content/uploads/image2/"
                
                //download index file first. it will contain the filename of the latest backup
                ftpDownloader.downloadFile(local_index_filename, index_filename, remote_dir);

                //check if index file download succeeded
                File index_file=new File(local_index_filename);
                if(index_file.exists())
                    remote_filename=FileReader.read(local_index_filename);

                if(remote_filename.length()==0)
                {
                    System.err.println("Either no backup exists or error downloading index file\n");
                }
                else
                {
                    System.out.println("Latest backup file is "+remote_filename);

                    //download latest backup file
                    ftpDownloader.downloadFile(local_filename, remote_filename, remote_dir);
                    ftpDownloader.disconnect();

                    File newfile=new File(local_filename);
                    if(existingfile.exists())
                    {
                        System.out.println("Restoring database, please wait");
                        DatabaseRestoreTool.restoreDB(database, username ,  password,  local_filename);
                    }
                    else
                    {
                        System.err.println("Error downloading database file");
                    }

                }
                    
		System.out.println("Done");
        } catch (Exception ex) {
                ex.printStackTrace();
        }
    }
    
}
