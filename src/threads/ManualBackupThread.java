/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import databasebackup.FrmMain;
import static threads.AutoBackupThread.running;

/**
 *
 * @author jaspertomas
 */
public class ManualBackupThread implements Runnable {
    String database;

    public ManualBackupThread(String database) {
        this.database = database;
    }
    
    @Override
    public void run() {
            //prevent two backup/restore threads from running at once
            if(running)
            {
                System.out.println("Another process is already running");
                return;
            }

            running=true;
        
            AutoBackupThread.backupAndUpload(database);
            
            //reenable buttons in form main
            FrmMain.getInstance().setButtonsEnabled(true);
            
            running=false;
    }
    
}
