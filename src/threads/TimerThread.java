package threads;

import databasebackup.FrmMain;
import models.Settings;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaspertomas
 */
public class TimerThread implements Runnable{

//    static TimerThread instance;
//
//    public static TimerThread getInstance() {
//        if(instance==null)
//            instance=new TimerThread();
//        return instance;
//    }
//    
    public TimerThread()
    {
//        instance=this;
//        running=false;
        minutes=Integer.valueOf(Settings.getByName("backupeveryminutes").value);
        countdown=minutes;
        secondscountdown=60;
    }
    
    static private Boolean running=false;
    Integer minutes;
    Integer countdown;
    Integer secondscountdown;
    public static void stop() {
        running=false;
    }

    @Override
    public void run() {
        
        //if there's already another thread running, quit
        if(running)return;
        
        running=true;
        while(running)
        {
            try {
                FrmMain.getInstance().setStatus("Next backup in "+countdown.toString()+" minutes");
                Thread.sleep(1000);
                
                secondscountdown--;
                
                if(secondscountdown==0)
                {
                    secondscountdown=60;
                    countdown--;
                }
                
                if(countdown==0)
                {
                    countdown=minutes;
                    new Thread(new AutoBackupThread()).start();
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        FrmMain.getInstance().setStatus("Automatic backup is off");
    
    }
}
