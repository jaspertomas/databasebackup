/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import databasebackup.FrmMain;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import utils.FTPUploader;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Settings;
import utils.DatabaseBackupTool;
import utils.fileaccess.FileWriter;

/**
 *
 * @author jaspertomas
 */
public class AutoBackupThread implements Runnable{

    static Boolean running=false;

    public static Boolean getRunning() {
        return running;
    }

    public static void setRunning(Boolean running) {
        AutoBackupThread.running = running;
    }

    @Override
    public void run() {
        //prevent two backup/restore threads from running at once
        if(running)
        {
            System.out.println("Another process is already running");
            return;
        }

        running=true;

        //for each database, do backup and upload
        String[] databases=Settings.getByName("databases").value.split(" ");
        for(String database:databases)
        {
            System.out.println("Backing up database "+database);
            backupAndUpload(database);
        }
        running=false;
    }
    public static void backupAndUpload(String database)
    {
        //get a timestamp
        Date d = new Date();
        String timestamp = new SimpleDateFormat ("yyyy-MM-dd:HH-mm-ss").format (d);

        String username=Settings.getByName("dbusername").value;
        String password=Settings.getByName("dbpassword").value;
        
        String local_filename=System.getProperty("user.dir")+"/backup.sql";
        String remote_filename=database+"-"+timestamp+".sql";
        remote_filename=remote_filename.replace(":", "-");
        String remote_dir="./";
        String index_filename=database+".txt";
        
        String ftp_password=Settings.getByName("ftppassword").value;
        String ftp_host=Settings.getByName("ftpserver").value;
        String ftp_user=Settings.getByName("ftpusername").value;

        try {
		System.out.println("Start");
                
                //write an index file containing the remote filename 
                //to easily find out the name of the latest uploaded file
                //when its time to restore
                boolean indexresult=FileWriter.write(index_filename, remote_filename);
                if(indexresult==false)
                {
                    System.err.println("Error writing index file; This will prevent restoration of this version");
                }
                
                //delete old file if exists
                File existingfile=new File(local_filename);
                if(existingfile.exists())existingfile.delete();

                System.out.println("Creating database backup");
                boolean backupresult=DatabaseBackupTool.backupDB(database, username ,  password,  local_filename);

                //File newfile=new File(local_filename);
                if(existingfile.exists() && backupresult==true)
                {
                    System.out.println("Database backup successful");
                    System.out.println("Uploading database backup");
                    FTPUploader ftpUploader = new FTPUploader(ftp_host, ftp_user, ftp_password);
                    //FTP server path is relative. So if FTP account HOME directory is "/home/pankaj/public_html/" and you need to upload 
                    // files to "/home/pankaj/public_html/wp-content/uploads/image2/", you should pass directory parameter as "/wp-content/uploads/image2/"
                    ftpUploader.uploadFile(local_filename, remote_filename, remote_dir);
                    
                    //if no problem creating index file
                    if(indexresult==true)
                    {
                        //upload index file too
                        ftpUploader.uploadFile(System.getProperty("user.dir")+"/"+index_filename, index_filename, remote_dir);

                        //delete index file if it exists
//                        File index_file=new File(System.getProperty("user.dir")+"/"+index_filename);
//                        if(index_file.exists())index_file.delete();
                    }
                    ftpUploader.disconnect();
                }
                else
                {
                    System.err.println("Error backing up database "+database);
                }

                System.out.println("Done");
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
