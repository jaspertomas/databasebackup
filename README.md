This is an automated database dump generator and uploader for MySql
It uploads to an FTP server of your choice
It automatically generates a database backup every 30 minutes by default
It can also do so at the click of a button.
It can also download and restore databases